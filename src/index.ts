export * from './Client'
export { plugin } from './lib/plugin'
export * from './types/Application'

export const version: string = require('../package.json').version
