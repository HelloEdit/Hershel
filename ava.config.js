export default {
  files: ['./test/**/*.test.ts'],
  require: ['ts-node/register/transpile-only'],
  compileEnhancements: false,
  extensions: ['ts'],
  verbose: true,
  failFast: true,
  cache: false
}
